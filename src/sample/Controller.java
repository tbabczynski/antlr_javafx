package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTreeNodeStream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class Controller {
    public TextArea src;
    public TextArea ast;
    public TextArea res;
    public Button ParseBtn;
    public Button PTBtn;

    private CommonTreeNodeStream nodes=null;

    /**
     * doParse
     * @param actionEvent
     * @throws IOException
     */
    public void doParse(ActionEvent actionEvent) throws IOException {
        ExprParser.prog_return root = runParser();

        // Wypisujemy drzewo na standardowe wyjście
//            System.out.println(((CommonTree)root.tree).toStringTree());
            ast.setText(root.tree.toStringTree());

            // Tworzymy bufor na węzły drzewa
             nodes = new CommonTreeNodeStream(root.tree);
             PTBtn.setDisable(false);
    }

    private ExprParser.prog_return runParser() {
        // Tworzymy analizator leksykalny i każemy mu czytać z stdin
        ANTLRStringStream input = new ANTLRStringStream(src.getText());
        ExprLexer lexer = new ExprLexer(input);

        // Tworzymy bufor na tokeny pomiędzy analizatorem leksykalnym a parserem
        CommonTokenStream tokens = new CommonTokenStream(lexer);


        // Tworzymy parser czytający z powyższego bufora
        ExprParser parser = new ExprParser(tokens);


        try {
            // Wywołujemy parser generujący drzewo startując od reguły prog (Z klasy Expr)
            return parser.prog();
        } catch (RecognitionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void run_tree_parser(ActionEvent actionEvent) {
        // Tworzymy parser drzew korzystający z powyższego bufora
        TExpr1 walker = new TExpr1(nodes);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        PrintStream old = System.out;
        System.setOut(ps);

        // Wywołujemy parser drzew - startując od reguły prog (Tym razem z klasy TExpr1!)
        try {
            walker.prog();
        } catch (RecognitionException e) {
            e.printStackTrace();
        }

        res.setText(baos.toString());
        System.setOut(old);
        nodes.reset();
    }

}
